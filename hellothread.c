#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

void *worker(void *);
int n;

int main (int argc, char** argv){

n = atoi(argv[1]);

if (argc <= 1){
	printf("Please run again and enter an integer\n");
	exit(1);
	}

if (n >= 1 && n <= 10){

	pthread_t thread_id[n];
	for (int i = 0; i < n; i++){
		pthread_create(&thread_id[i], NULL, worker, (void *) 				&i);
		printf("Creating thread %d\n",i+1);
		} 
	for (int j = 0; j < n; ++j){
		pthread_join(thread_id[j], NULL);
		}
	}
else {	
	printf ("Error please enter an interger between 1-10\n");
	exit(1);
	}
}

void *worker( void *arg) {
int *id = (int *)arg;
printf ("Hello World! I am thread %d\n", *id);
pthread_exit(0);
}

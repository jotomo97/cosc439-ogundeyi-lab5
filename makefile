CC = gcc 
CFLAGS = -g -Wall -pedantic -pthread


TARGET = 

all: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c

